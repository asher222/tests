import sys
import pickle 

want = [42, 0]

def lookingFor(i, want):
  if i in want:
    return True

def thingsTrue(lst, want):
  trueThings = []
  for i in lst:
    if lookingFor(i, want):
      trueThings.append(i)      
  return trueThings

if __name__ == "__main__":
  test = [42, 99, 1, 122, 2348, 0]
  output = thingsTrue(test, want)
  sys.stdout.write(str(output))
  pickle.dump(output, open("lookingFor.p","wb"))

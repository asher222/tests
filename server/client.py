#!/usr/bin/env python

import socket

TCP_IP = '127.0.0.1'
TCP_PORT = 10000
BUFFER_SIZE = 1024
MESSAGE = "Hello, World!"

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# client_address = ('localhost', 10123)
# s.bind(client_address)

# DNS lookup
# 127.0.0.1 = localhost = this computer
# You don't have to bind your client socket to a port

s.connect((TCP_IP, TCP_PORT))
s.send(MESSAGE)
data = s.recv(BUFFER_SIZE)
s.close()

print "received data:", data

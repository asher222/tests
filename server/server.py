import socket
import sys

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Bind the socket to the port
server_address = ('localhost', 10000)
print >>sys.stderr, 'starting up on %s port %s' %server_address
sock.bind(server_address)

# Acting as a server
# Listen for incoming connections
sock.listen(1)

while True:
    # Wait for a connection
    print >> sys.stderr, 'waiting for a connection'
    connection, client_address = sock.accept()

    try:
        print('trying')
        print >>sys.stderr, 'connection from', client_address

        # Receive the data in small chunks and retransmit it
        while True:
            data = connection.recv(16)
            print >>sys.stderr, 'received "%s"' % data
            if data:
                print >>sys.stderr, 'sending data back to the client'
                var = str(client_address)
                connection.sendall(var)
            else:
                print >>sys.stderr, 'no more data from', client_address
                break
            
    finally:
        # Clean up the connection
        connection.close()


# Learn about how TCP/IP are related to stdout, stderr, and http
# How are they related: practically, conceptually
# Especially HTTP
# Modify the server so it sends back data that a client that expects
# HTTP could handle
# Learn what HTTP clients can process. How do you talk?
# Modify the client using sockets and low-level library tools
# Send an HTTP request and obtain the correct response
# Push
# Study comparisons between serialization and TCP/IP
# Read documentation on sys.std
# Read about files in general, reading and writing
# There is no magic

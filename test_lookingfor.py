import unittest
from lookingfor import lookingFor

class LookingForTestCase(unittest.TestCase):
    def test_is_5_what_i_want(self):
        self.assertTrue(lookingFor(5, want=[5,6]))

if __name__ == "__main__":
	unittest.main()

import unittest
from lookingfor import *

class ThingsTrueTestCase(unittest.TestCase):
    def test_list_against_want(self):
        want = [42, 6]
        self.assertTrue(thingsTrue([1,2,3,4,5,6], want))

if __name__ == "__main__":
    unittest.main()
